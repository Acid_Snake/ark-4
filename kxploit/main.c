#include <sdk.h>
#include "kxploit.h"
#include "flashpatch.h"
#include "macros.h"

void p5_open_savedata(int mode)
{
	p5_close_savedata();

	SceUtilitySavedataParam dialog;

	memset(&dialog, 0, sizeof(SceUtilitySavedataParam));
	dialog.base.size = sizeof(SceUtilitySavedataParam);

	dialog.base.language = 1;
	dialog.base.buttonSwap = 1;
	dialog.base.graphicsThread = 0x11;
	dialog.base.accessThread = 0x13;
	dialog.base.fontThread = 0x12;
	dialog.base.soundThread = 0x10;

	dialog.mode = mode;

	g_tbl->UtilitySavedataInitStart(&dialog);

	// Wait for the dialog to initialize
	while (g_tbl->UtilitySavedataGetStatus() < 2)
	{
		g_tbl->KernelDelayThread(100);
	}
}

// Runs the savedata dialog loop
void p5_close_savedata()
{

	int running = 1;
	int last_status = -1;

	while(running) 
	{
		int status = g_tbl->UtilitySavedataGetStatus();
		
		if (status != last_status)
		{
			last_status = status;
		}

		switch(status) 
		{
			case PSP_UTILITY_DIALOG_VISIBLE:
				g_tbl->UtilitySavedataUpdate(1);
				break;

			case PSP_UTILITY_DIALOG_QUIT:
				g_tbl->UtilitySavedataShutdownStart();
				break;

			case PSP_UTILITY_DIALOG_NONE:
				running = 0;
				break;

			case PSP_UTILITY_DIALOG_FINISHED:
				break;
		}
		g_tbl->KernelDelayThread(100);
	}
}

int bootRecoveryCheck(){
	// Allocate Buffer for Gamepad Data
	SceCtrlData data;
		
	// Set Sampling Cycle
	k_tbl->KernelCtrlSetSamplingCycle(0);
	
	// Set Sampling Mode (we don't need the analog stick really)
	k_tbl->KernelCtrlSetSamplingMode(PSP_CTRL_MODE_DIGITAL);
		
	// Poll 64 Times
	int i = 0; for(; i < 64; i++)
	{
		// Clear Memory
		memset(&data, 0, sizeof(data));
			
		// Poll Data
		k_tbl->KernelCtrlPeekBufferPositive(&data, 1);
			
		// Recovery Mode
		if((data.Buttons & PSP_CTRL_RTRIGGER) == PSP_CTRL_RTRIGGER)
		{
		
			return 1;
		}
	}
	
	// Normal Boot
	return 0;
}

void flashPatch(){
	if (IS_PSP)
		return; // flash0 will already be installed
	if (NEWER_FIRMWARE){
		// Redirect KERMIT_CMD_ERROR_EXIT loadFlash function
		u32 knownnids[2] = { 0x3943440D, 0x0648E1A3 /* 3.3X */ };
		u32 swaddress = 0;
		int i;
		for (i = 0; i < 2; i++){
			swaddress = findFirstJALForFunction("sceKermitPeripheral_Driver", "sceKermitPeripheral_driver", knownnids[i]);
			if (swaddress != 0)
				break;
		}
		_sw(JUMP(flashLoadPatch), swaddress);
		_sw(NOP, swaddress+4);
	}
	else{
		// Patch flash0 Filesystem Driver
		if(patchFlash0Archive() < 0){
			return;
		}
	}
}

void initKxploit()__attribute__((section(".text.startup")));
void initKxploit(){
	kxf->stubScanner = &stubScanner;
	kxf->doExploit = &doExploit;
	kxf->executeKernel = &executeKernel;
	kxf->repairInstruction = &repairInstruction;
	kxf->p5_open_savedata = &p5_open_savedata;
	kxf->p5_close_savedata = &p5_close_savedata;
	kxf->bootRecoveryCheck = &bootRecoveryCheck;
}
