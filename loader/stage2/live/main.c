/*
 * This file is part of PRO CFW.

 * PRO CFW is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * PRO CFW is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PRO CFW. If not, see <http://www.gnu.org/licenses/ .
 */

#include "main.h"
#include <loadexec_patch.h>
#include "libs/graphics/graphics.h"
#include "flash_dump.h"
#include "reboot.h"
#include <functions.h>

char* savepath = (char*)0x08803000;
char* running_ark = "Running ARK-4 in ?PS? mode";
char* test_eboot = "ms0:/EBOOT.PBP";
char* menu_boot = "?BOOT.PBP";

// Sony Reboot Buffer Loader
int (* _LoadReboot)(void *, unsigned int, void *, unsigned int) = NULL;

// LoadExecVSHWithApitype Direct Call
int (* _KernelLoadExecVSHWithApitype)(int, char *, struct SceKernelLoadExecVSHParam *, int) = NULL;

// K.BIN entry point
void (* kEntryPoint)() = (void*)KXPLOIT_LOADADDR;

void initKxploitFile(){
	char k_path[SAVE_PATH_SIZE];
	strcpy(k_path, savepath);
	strcat(k_path, "K.BIN");
	
	SceUID fd = g_tbl->IoOpen(k_path, PSP_O_RDONLY, 0);
	g_tbl->IoRead(fd, (void *)(KXPLOIT_LOADADDR|0x40000000), 0x4000);
	g_tbl->IoClose(fd);
	kEntryPoint();
}

void detectPSPMode(){
	char* test_eboot = "ms0:/EBOOT.PBP";
	// determine if in PSP (otherwise we're on ePSP)
	SceUID fd = g_tbl->IoOpen(test_eboot, PSP_O_CREAT|PSP_O_TRUNC|PSP_O_WRONLY, 0777);
	g_tbl->IoClose(fd);
	fd = g_tbl->IoOpen(test_eboot, PSP_O_RDONLY, 0777);
	g_tbl->is_real_psp = (fd >= 0);
	g_tbl->IoClose(fd);
}

// Entry Point
int exploitEntry(char* arg0) __attribute__((section(".text.startup")));
int exploitEntry(char* arg0){

	// copy the path of the save
	strcpy(savepath, arg0);
	
	// Clear BSS Segment
	clearBSS();

	// init function table
	getUserFunctions();

	// make PRTSTR available for payloads
	g_tbl->prtstr = (void *)&PRTSTR11;

	// init screen
	initScreen(g_tbl->DisplaySetFrameBuf);

	// Output Exploit Reach Screen
	PRTSTR("Starting");
	
	// read kxploit file into memory and initialize it
	initKxploitFile();
	
	// detect if we're on a real PSP or ePSP
	detectPSPMode();
	
	char* err = NULL;
	
	if (kxf->stubScanner() == 0){
		// Corrupt Kernel
		if (kxf->doExploit() >= 0){
			// Flush Cache
			g_tbl->KernelDcacheWritebackAll();

			// Refresh screen (vram buffer screws it)
			cls();
			
			// Output Loading Screen
			PRTSTR("Loading");
			
			// Trigger Kernel Permission Callback
			kxf->executeKernel((u32)&kernelContentFunction);
		}
		else{
			err = "Exploit failed";
		}
	}
	else{
		err = "Scan failed";
	}
	
	PRTSTR(err);
	_sw(0,0);

	return 0;
}

#if FLASH_DUMP == 0

void runMenu(void){
	PRTSTR("running menu");

	menu_boot[0] = (kxf->bootRecoveryCheck(k_tbl))? 'R' : (IS_VITA_POPS)? 'X' : 'V';

	char path[SAVE_PATH_SIZE];
	memset(path, 0, SAVE_PATH_SIZE);
	strcpy(path, savepath);
	strcat(path, menu_boot);
	
	// Prepare Homebrew Reboot
	char * ebootpath = path;
	struct SceKernelLoadExecVSHParam param;
	memset(&param, 0, sizeof(param));
	param.size = sizeof(param);
	param.args = strlen(ebootpath) + 1;
	param.argp = ebootpath;
	param.key = "game";

	// Trigger Reboot
	_KernelLoadExecVSHWithApitype(0x141, ebootpath, &param, 0x10000);
}
#endif

void determineGlobalConfig(){

	SceUID fd = -1;

	// copy savepath
	memset(ark_config->savepath, 0, SAVE_PATH_SIZE);
	strcpy(ark_config->savepath, savepath);
	
	k_tbl->KernelIORemove(test_eboot);
	
	// determine execution mode (real PSP, ePSP, ePSX)
	ark_config->exec_mode = (g_tbl->is_real_psp)? REAL_PSP :
		(k_tbl->KernelFindModuleByName("pspvmc_Library") != NULL)? EPSX : EPSP;

	setIsVitaPops((int)IS_VITA_POPS);
	
	// detect if can make folder in PSP/GAME
	if (!g_tbl->is_real_psp && !IS_VITA_POPS){
		k_tbl->KernelIOMkdir("ms0:/PSP/GAME/ARKTESTFOLDER", 0777);
		ark_config->can_install_game = k_tbl->KernelIORmdir("ms0:/PSP/GAME/ARKTESTFOLDER") == 0;
	}
	
	// find game ID
	if (IS_VITA_POPS){
		*((u8*)GAMEID) = 0;
	}
	else{
		fd = k_tbl->KernelIOOpen("disc0:/UMD_DATA.BIN", PSP_O_RDONLY, 0777);
		u8 skip;
	
		k_tbl->KernelIORead(fd, GAMEID, 4);
		k_tbl->KernelIORead(fd, &skip, 1);
		k_tbl->KernelIORead(fd, GAMEID+4, 5);
	
		k_tbl->KernelIOClose(fd);
	}
}

// Kernel Permission Function
void kernelContentFunction(void){
	// Switch to Kernel Permission Level
	setK1Kernel();

	kxf->repairInstruction();
	
	// get kernel functions
	getKernelFunctions();
	
	// determine global configuration that affects how ARK behaves
	determineGlobalConfig();
	
	#if FLASH_DUMP == 1
	initKernelThread();
	return;
	#else
	running_ark[17] = (IS_PSP)? ' ' : 'e';
	running_ark[20] = (IS_VITA_POPS)? 'X':'P';
	PRTSTR(running_ark);

	// Find LoadExec Module
	SceModule2 * loadexec = k_tbl->KernelFindModuleByName("sceLoadExec");
	
	// Find Reboot Loader Function
	_LoadReboot = (void *)loadexec->text_addr;
	
	// Find LoadExec Functions
	_KernelLoadExecVSHWithApitype = (void *)findFirstJALForFunction("sceLoadExec", "LoadExecForKernel", 0xD8320A28);
	
	// make the common loadexec patches
	patchLoadExecCommon(loadexec, (u32)LoadReboot);

	kxf->flashPatch();

	// Invalidate Cache
	k_tbl->KernelIcacheInvalidateAll();
	k_tbl->KernelDcacheWritebackInvalidateAll();

	runMenu();
	#endif
}

// Fake K1 Kernel Setting
void setK1Kernel(void){
	// Set K1 to Kernel Value
	__asm__ (
		"nop\n"
		"lui $k1,0x0\n"
	);
}

// Clear BSS Segment of Payload
void clearBSS(void){
	// BSS Start and End Address from Linkfile
	extern char __bss_start, __bss_end;
	
	// Clear Memory
	memset(&__bss_start, 0, &__bss_end - &__bss_start);
}
