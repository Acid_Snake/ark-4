#include <pspsdk.h>
#include <pspdebug.h>
#include <pspiofilemgr.h>
#include <pspctrl.h>
#include <pspinit.h>
#include <pspdisplay.h>
#include <pspkernel.h>
#include <psputility.h>
#include <malloc.h>
#include <string.h>
#include <stdio.h>
#include <graphics.h>

PSP_MODULE_INFO("ARK Loader", PSP_MODULE_USER, 1, 0);
PSP_MAIN_THREAD_ATTR(PSP_THREAD_ATTR_USER | PSP_THREAD_ATTR_VFPU);
PSP_HEAP_SIZE_KB(4096);

#include "ark_imports.h"

#define ARK_LOADADDR 0x00010000 //0x08D20000
#define ARK_BIN "ark.bin" //"ARK.BIN"
#define INSTALL_PATH "ms0:/PSP/SAVEDATA/ARK_01234/"

int SysMemUserForUser_91DE343C(void* unk);

// ARK's linkless loader must be able to find this string
static const char* loadpath = INSTALL_PATH ARK_BIN;

// ARK.BIN requires these imports
void loadARKImports(){
	IoOpen = &sceIoOpen;
	IoRead = &sceIoRead;
	IoClose = &sceIoClose;
	IoWrite = &sceIoWrite;
	
	KernelLibcTime = &sceKernelLibcTime;
	KernelDcacheWritebackAll = &sceKernelDcacheWritebackAll;
	DisplaySetFrameBuf = &sceDisplaySetFrameBuf;

	KernelCreateThread = &sceKernelCreateThread;
	KernelDelayThread = &sceKernelDelayThread;
	KernelStartThread = &sceKernelStartThread;
	KernelWaitThreadEnd = &sceKernelWaitThreadEnd;

	KernelDeleteVpl = &sceKernelDeleteVpl;
	KernelDeleteFpl = &sceKernelDeleteFpl;
	
	UtilityLoadModule = &sceUtilityLoadModule;
	UtilityUnloadModule = &sceUtilityUnloadModule;
	UtilityLoadNetModule = &sceUtilityLoadNetModule;
	UtilityUnloadNetModule = &sceUtilityUnloadNetModule;
	
	_SysMemUserForUser_91DE343C = &SysMemUserForUser_91DE343C;
	KernelFreePartitionMemory = &sceKernelFreePartitionMemory;
	
	KernelCpuSuspendIntr = (void*)&sceKernelCpuSuspendIntr;
	KernelCpuResumeIntr = (void*)&sceKernelCpuResumeIntr;
	
	_sceUtilitySavedataGetStatus = (void*)&sceUtilitySavedataGetStatus;
	_sceUtilitySavedataInitStart = (void*)&sceUtilitySavedataInitStart;
	_sceUtilitySavedataUpdate = (void*)&sceUtilitySavedataUpdate;
	_sceUtilitySavedataShutdownStart = (void*)&sceUtilitySavedataShutdownStart;
}

int main(){
	loadARKImports();
	
	SceUID fd = sceIoOpen(loadpath, PSP_O_RDONLY, 0);
	sceIoRead(fd, (void *)(ARK_LOADADDR), 0x4000);
	sceIoClose(fd);
	
	sceKernelDcacheWritebackAll();
	
	void (* hEntryPoint)(char*) = (void*)ARK_LOADADDR;
	hEntryPoint(INSTALL_PATH);
	
	return 0;
}
